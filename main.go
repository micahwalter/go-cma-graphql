package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/graphql-go/graphql"
	gqlhandler "github.com/graphql-go/graphql-go-handler"
	"github.com/mnmtanish/go-graphiql"
)

// Artwork is an artwork
type Artwork struct {
	Data struct {
		ID                       int    `json:"id"`
		AccessionNumber          string `json:"accession_number,omitempty"`
		ShareLicenseStatus       string `json:"share_license_status,omitempty"`
		Tombstone                string `json:"tombstone,omitempty"`
		CurrentLocation          string `json:"current_location,omitempty"`
		Title                    string `json:"title,omitempty"`
		TitleInOriginalLanguage  string `json:"title_in_original_language,omitempty"`
		Series                   string `json:"series,omitempty"`
		SeriesInOriginalLanguage string `json:"series_in_original_language,omitempty"`
		CreationDate             string `json:"creation_date,omitempty"`
		CreationDateEarliest     int    `json:"creation_date_earliest,omitempty"`
		CreationDateLatest       int    `json:"creation_date_latest,omitempty"`
	} `json:"data"`
}

func determineListenAddress() (string, error) {
	port := os.Getenv("PORT")

	if port == "" {
		//return "", fmt.Errorf("$PORT not set")
		port = "3000"
	}

	return ":" + port, nil
}

func createQueryType(artworkType *graphql.Object) graphql.ObjectConfig {
	return graphql.ObjectConfig{Name: "QueryType", Fields: graphql.Fields{
		"artwork": &graphql.Field{
			Type: artworkType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				id := p.Args["id"]
				v, _ := id.(int)
				log.Printf("fetching artwork with id: %d", v)
				rsp, err := fetchArtworkByiD(v)
				return rsp.Data, err
			},
		},
	}}
}

func createArtworkType() *graphql.Object {
	return graphql.NewObject(graphql.ObjectConfig{
		Name: "Artwork",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.NewNonNull(graphql.Int),
			},
			"accession_number": &graphql.Field{
				Type: graphql.String,
			},
			"share_license_status": &graphql.Field{
				Type: graphql.String,
			},
			"tombstone": &graphql.Field{
				Type: graphql.String,
			},
			"current_location": &graphql.Field{
				Type: graphql.String,
			},
			"title": &graphql.Field{
				Type: graphql.String,
			},
			"title_in_original_language": &graphql.Field{
				Type: graphql.String,
			},
			"series": &graphql.Field{
				Type: graphql.String,
			},
			"series_in_original_language": &graphql.Field{
				Type: graphql.String,
			},
			"creation_date": &graphql.Field{
				Type: graphql.String,
			},
			"creation_date_earliest": &graphql.Field{
				Type: graphql.Int,
			},
			"creation_date_latest": &graphql.Field{
				Type: graphql.Int,
			},
		},
	})
}

func fetchArtworkByiD(id int) (*Artwork, error) {

	resp, err := http.Get(fmt.Sprintf("https://openaccess-api.clevelandart.org/api/artworks/%d", id))

	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("%s: %s", "could not fetch data", resp.Status)
	}

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.New("could not read data")
	}

	result := Artwork{}
	err = json.Unmarshal(b, &result)
	if err != nil {
		return nil, errors.New("could not unmarshal data")
	}

	return &result, nil
}

func main() {

	schema, err := graphql.NewSchema(graphql.SchemaConfig{
		Query: graphql.NewObject(
			createQueryType(
				createArtworkType(),
			),
		),
	})

	if err != nil {
		log.Fatalf("failed to create new schema, error: %v", err)
	}

	addr, err := determineListenAddress()

	handler := gqlhandler.New(&gqlhandler.Config{
		Schema: &schema,
	})

	http.Handle("/graphql", handler)
	http.HandleFunc("/", graphiql.ServeGraphiQL)

	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Listening on %s...\n", addr)

	if err := http.ListenAndServe(addr, nil); err != nil {
		panic(err)
	}

}
